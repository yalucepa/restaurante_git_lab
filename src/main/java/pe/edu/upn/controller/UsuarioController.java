package pe.edu.upn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import pe.edu.upn.model.entity.Usuario;
import pe.edu.upn.service.UsuarioService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;
	@GetMapping
	public String inicio(Model model) {
		try {
			List<Usuario> usuarios = usuarioService.findAll();
			model.addAttribute("usuarios", usuarios);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "/usuario/inicio";
	}

	@GetMapping("/lista")
public String inicioLista(Model model) {
	try {
		List<Usuario> usuarios = usuarioService.findAll();
		model.addAttribute("usuarios", usuarios);
	} catch (Exception e) {
		// TODO: handle exception
	}
	return "/usuario/lista";
}

}